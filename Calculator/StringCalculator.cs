﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var stringNumbersArray = GetSplittedDelimiteredStringNumbers(numbers);
            var numbersList = GetConvertedNumbersInRange(stringNumbersArray);
            
            return Sum(numbersList);
        }

        private string[] GetSplittedDelimiteredStringNumbers(string numbers)
        {
            if (numbers.StartsWith("//")) 
            {
                return SplitDelimiteredStringNumbers(numbers);
            }
            
            return numbers.Split(new char[] {',','\n'});
        }

        private string[] SplitDelimiteredStringNumbers(string numbers)
        {
            var numbersAndDelimiters = numbers.Split("\n");
            numbersAndDelimiters[0] = numbersAndDelimiters[0].Replace("//","");

            if (numbers[2] == '[')
            {
                var delimiters = numbersAndDelimiters[0].Split(new char[] { '[', ']'}, StringSplitOptions.RemoveEmptyEntries);
                return numbersAndDelimiters[1].Split(delimiters, StringSplitOptions.None);
            }
            
            return numbersAndDelimiters[1].Split(numbersAndDelimiters[0], StringSplitOptions.None);
        }

        private List<int> GetConvertedNumbersInRange(string[] stringNumbersArray)
        {
            var numbersList = new List<int>();
            var negativeNumbersList = new List<int>();

            foreach(var number in stringNumbersArray)
            {
                var convertedNumber = int.Parse(number);

                if (convertedNumber > 0 && convertedNumber < 1001)
                {
                    numbersList.Add(convertedNumber);
                    continue;
                }

                if (convertedNumber < 0)
                {
                    negativeNumbersList.Add(convertedNumber);
                }
            }

            CheckForNegativeNumbers(negativeNumbersList);

            return numbersList;
        }

        private void CheckForNegativeNumbers(List<int> negativeNumbersList)
        {
            if (negativeNumbersList.Count > 0)
            {
               throw new Exception("Negatives not allowed: " + string.Join(",", negativeNumbersList));
            }
        }
        
        private int Sum(List<int> numbersList)
        {
            int sum = 0; 

            foreach(var number in numbersList)
            {
                sum += number;
            }

            return sum;
        }
    }
}
